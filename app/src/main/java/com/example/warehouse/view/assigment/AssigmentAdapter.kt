/*
 * Created by Aleksandar Ilić on 11/15/20 9:29 PM
 */

package com.example.warehouse.view.assigment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.warehouse.R
import com.example.warehouse.service.model.Assigment
import kotlinx.android.synthetic.main.recyclerview_item.view.*

class AssigmentAdapter(context: Context, private var assigments: List<Assigment>, private val assigmentClickListener: AssigmentClickListener):
        RecyclerView.Adapter<AssigmentAdapter.AssigmentViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var assigmentList = listOf<Assigment>()

    init {
        assigmentList = assigments
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AssigmentAdapter.AssigmentViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return AssigmentViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: AssigmentAdapter.AssigmentViewHolder, position: Int) {
        val current = assigmentList[position]
        holder.assigment.text = current.name
        holder.status.text = current.completed

        holder.itemView.setOnClickListener { assigmentClickListener.onAssigmentClickListener(current) }
    }

    override fun getItemCount(): Int = assigmentList.size

    inner class AssigmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val assigment: TextView = itemView.assigmentTv
        val status: TextView = itemView.statusTv
    }
    interface AssigmentClickListener {
        fun onAssigmentClickListener(assigment: Assigment)
    }
}