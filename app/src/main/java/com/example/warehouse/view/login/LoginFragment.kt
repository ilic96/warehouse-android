/*
 * Created by Aleksandar Ilić on 11/13/20 9:05 PM
 */

package com.example.warehouse.view.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.warehouse.R
import com.example.warehouse.service.model.LoginCredentials
import com.example.warehouse.view.assigment.AssigmentActivity
import com.example.warehouse.view.assigment.AssigmentFragment
import com.example.warehouse.viewmodel.LoginViewModel
import com.github.ajalt.timberkt.d
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.login_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginViewModel = ViewModelProvider(requireActivity(), ViewModelProvider.AndroidViewModelFactory(requireActivity().application)).get(LoginViewModel::class.java)

        btnLogin.setOnClickListener {
            lifecycleScope.launchWhenCreated {Dispatchers.IO
                val usrname = username.text.toString()
                val pswd = password.text.toString()
                try {
                    val loginResult = loginViewModel.login(GRANT_TYPE, usrname, pswd)
                    if (loginResult.token.isNotEmpty()) {
                        tempToken = loginResult.token
                        if  (rememberCheck.isChecked) {
                            val credentials = LoginCredentials(1, usrname, pswd,   true)
                            loginViewModel.insertCredentials(credentials)
                        }
                        val intent = Intent(requireActivity(), AssigmentActivity::class.java)
                        intent.putExtra(AssigmentFragment.TOKEN, loginResult)
                        startActivity(intent)
                    }
                } catch (ex: Exception) {
                    Toast.makeText(requireActivity(), "Check your credentials and try again.", Toast.LENGTH_SHORT).show()
                    d {"[flow] ${ex.localizedMessage}"}
                }
            }
        }
    }
    companion object {
        fun newInstance() = LoginFragment()
        var tempToken: String? = LoginFragment::class.java.canonicalName
        private const val GRANT_TYPE = "password"
    }
}