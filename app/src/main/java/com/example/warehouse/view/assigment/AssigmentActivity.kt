/*
 * Created by Aleksandar Ilić on 11/15/20 9:29 PM
 */

package com.example.warehouse.view.assigment

import androidx.fragment.app.Fragment
import com.example.warehouse.core.SingleFragmentActivity

class AssigmentActivity : SingleFragmentActivity() {
    override fun createFragment(): Fragment = AssigmentFragment.newInstance()
}
