/*
 * Created by Aleksandar Ilić on 11/16/20 1:19 PM
 */

package com.example.warehouse.view.assigment_details

import androidx.fragment.app.Fragment
import com.example.warehouse.core.SingleFragmentActivity

class AssigmentDetailsActivity : SingleFragmentActivity(){
    override fun createFragment(): Fragment = AssigmentDetailsFragment.newInstance()
}