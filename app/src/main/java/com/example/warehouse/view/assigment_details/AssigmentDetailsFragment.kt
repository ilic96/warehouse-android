/*
 * Created by Aleksandar Ilić on 11/16/20 1:20 PM
 */

package com.example.warehouse.view.assigment_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.warehouse.R
import com.example.warehouse.service.model.Assigment
import com.example.warehouse.view.login.LoginFragment
import com.example.warehouse.viewmodel.AssigmentViewModel
import kotlinx.android.synthetic.main.assigment_details_fragment.*
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class AssigmentDetailsFragment : Fragment() {

    private lateinit var assigmentViewModel: AssigmentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       return inflater.inflate(R.layout.assigment_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        assigmentViewModel = ViewModelProvider(requireActivity(), ViewModelProvider.AndroidViewModelFactory(requireActivity().application)).get(AssigmentViewModel::class.java)
        val response = activity?.intent?.getParcelableExtra<Assigment>(ASSIGMENT)
        assigmentName.text = response?.name
        assigmentDesc.text = response?.description
        assigmentStatus.text = response?.completed

        btnCompleteAssigment.setOnClickListener {
            try {
                response!!.completed = "True"
                val asg = Assigment(response.id, response.accountEmail,response.completed, response.description, response.name)
                lifecycleScope.launchWhenCreated {Dispatchers.IO
                    /*val credentials = assigmentViewModel.loadCredentials()*/
                    assigmentViewModel.doAssigment("Bearer ${LoginFragment.tempToken}", response.id, asg)
                }
            } catch (ex: Exception) {
                Toast.makeText(requireActivity(), ex.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }
    companion object {
        fun newInstance() = AssigmentDetailsFragment()
        const val ASSIGMENT = "com.example.warehouse.view.assigment_details.AssigmentDetailsFragment"
    }
}