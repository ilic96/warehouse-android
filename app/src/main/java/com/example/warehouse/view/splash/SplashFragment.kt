/*
 * Created by Aleksandar Ilić on 11/13/20 9:05 PM
 */

package com.example.warehouse.view.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.warehouse.R
import com.example.warehouse.view.assigment.AssigmentActivity
import com.example.warehouse.view.assigment.AssigmentFragment
import com.example.warehouse.view.login.LoginActivity
import com.example.warehouse.viewmodel.LoginViewModel
import kotlinx.coroutines.Dispatchers

class SplashFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.splash_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginViewModel = ViewModelProvider(requireActivity(), ViewModelProvider.AndroidViewModelFactory(requireActivity().application)).get(
            LoginViewModel::class.java)

        lifecycleScope.launchWhenCreated {
            Dispatchers.IO
            val check = loginViewModel.loadCredentials()
            if (check?.isLoggedIn == false || check == null) {
                showLoginScreen()
            } else {
                val loginResult = loginViewModel.login("password", check.usrname, check.pswd)
                val intent = Intent(requireActivity(), AssigmentActivity::class.java)
                intent.putExtra(AssigmentFragment.TOKEN, loginResult)
                startActivity(intent)
            }
        }
    }
    private fun showLoginScreen() {
        Handler(Looper.myLooper()!!).postDelayed({
            startActivity(Intent(requireActivity(), LoginActivity::class.java))
        }, 1000)
    }
    companion object {
        fun newInstance() = SplashFragment()
    }
}