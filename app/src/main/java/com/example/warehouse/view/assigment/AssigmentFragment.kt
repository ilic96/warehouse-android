/*
 * Created by Aleksandar Ilić on 11/15/20 9:29 PM
 */

package com.example.warehouse.view.assigment

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.warehouse.R
import com.example.warehouse.service.model.Assigment
import com.example.warehouse.service.model.LoginCredentials
import com.example.warehouse.service.model.LoginResponse
import com.example.warehouse.view.assigment_details.AssigmentDetailsActivity
import com.example.warehouse.view.assigment_details.AssigmentDetailsFragment
import com.example.warehouse.view.login.LoginActivity
import com.example.warehouse.viewmodel.AssigmentViewModel
import kotlinx.android.synthetic.main.assigment_fragment.*
import kotlinx.coroutines.Dispatchers

class AssigmentFragment : Fragment(), AssigmentAdapter.AssigmentClickListener {
    private lateinit var assigmentViewModel: AssigmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.assigment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.logoutMenu -> logout()
        }
        return super.onOptionsItemSelected(item)
    }
    private fun logout() {
        lifecycleScope.launchWhenCreated {Dispatchers.IO
            val credentialsToUpdate = LoginCredentials(1, "", "",  false)
            try {
                assigmentViewModel.insertCredentials(credentialsToUpdate)
                startActivity(Intent(requireActivity(), LoginActivity::class.java))
            } catch (ex: Exception) {
                Toast.makeText(requireActivity(), ex.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.assigment_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = "Daily assigments"
        assigmentViewModel = ViewModelProvider(requireActivity(), ViewModelProvider.AndroidViewModelFactory(requireActivity().application)).get(AssigmentViewModel::class.java)

        val result = requireActivity().intent.getParcelableExtra<LoginResponse>(TOKEN)
        (activity as AppCompatActivity).supportActionBar?.subtitle = "User ${result?.user}"

        lifecycleScope.launchWhenCreated {Dispatchers.IO
            try {
                val allAssigments = assigmentViewModel.getAssigments("Bearer ${result!!.token}", result.user).filter { it -> it.completed == "False" }
                assigmentViewModel.insertAllAssigments(allAssigments)

                val adapter = AssigmentAdapter(requireActivity(), allAssigments, this@AssigmentFragment)
                recyclerView.adapter = adapter
                recyclerView.layoutManager = LinearLayoutManager(requireActivity())
            } catch (ex: Exception) {
                Toast.makeText(requireActivity(), ex.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onAssigmentClickListener(assigment: Assigment) {
        lifecycleScope.launchWhenCreated {Dispatchers.IO
            try {
                val assigmentDetails = assigmentViewModel.loadAssigmentDetails(assigment.id)
                val intent = Intent(requireActivity(), AssigmentDetailsActivity::class.java)
                intent.putExtra(AssigmentDetailsFragment.ASSIGMENT, assigmentDetails)
                startActivity(intent)
            }
            catch (ex: Exception) {
                Toast.makeText(requireActivity(), ex.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        fun newInstance() = AssigmentFragment()
        const val TOKEN: String = "com.example.warehouse.view.assigment.AssigmentFragment"
    }
}