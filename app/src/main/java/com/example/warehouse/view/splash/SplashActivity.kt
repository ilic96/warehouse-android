
/*
 * Created by Aleksandar Ilić on 11/13/20 9:05 PM
 */

/*
 * Created by Aleksandar Ilić on 11/13/20 6:19 PM
 */

package com.example.warehouse.view.splash

import androidx.fragment.app.Fragment
import com.example.warehouse.core.SingleFragmentActivity

class SplashActivity : SingleFragmentActivity() {
    override fun createFragment(): Fragment = SplashFragment.newInstance()
}