
/*
 * Created by Aleksandar Ilić on 11/13/20 9:05 PM
 */

package com.example.warehouse.view.login

import androidx.fragment.app.Fragment
import com.example.warehouse.core.SingleFragmentActivity

class LoginActivity : SingleFragmentActivity() {
    override fun createFragment(): Fragment = LoginFragment.newInstance()
}