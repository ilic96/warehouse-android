/*
 * Created by Aleksandar Ilić on 11/13/20 7:38 PM
 */

package com.example.warehouse.core

import android.app.Application
import android.content.Context
import com.example.warehouse.BuildConfig
import com.example.warehouse.di.networkModule
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.bind
import org.kodein.di.singleton
import timber.log.Timber
import java.util.*

class Warehouse : Application(), DIAware{
    override val di = DI.lazy {
        bind<Context>("ApplicationContext") with singleton { this@Warehouse.applicationContext }
        bind<Warehouse>() with singleton { this@Warehouse }
        import(networkModule)
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}