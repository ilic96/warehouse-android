/*
 * Created by Aleksandar Ilić on 11/13/20 7:55 PM
 */

package com.example.warehouse.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.warehouse.service.model.LoginCredentials
import com.example.warehouse.service.model.LoginResponse
import com.example.warehouse.service.repository.WarehouseRepo
import kotlinx.coroutines.launch
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance

class LoginViewModel(application: Application) : AndroidViewModel(application), DIAware  {
    override val di by closestDI(application)
    private val repository: WarehouseRepo by instance()

     suspend fun login(grantType: String, usrname: String, pswd: String): LoginResponse  {
        return repository.login(grantType, usrname, pswd)
    }
    suspend fun loadCredentials() = repository.loadCredentials()
    suspend fun insertCredentials(credentials: LoginCredentials) = repository.insertCredentials(credentials)
}