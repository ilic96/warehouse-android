/*
 * Created by Aleksandar Ilić on 11/16/20 11:21 AM
 */

package com.example.warehouse.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.warehouse.service.model.Assigment
import com.example.warehouse.service.model.LoginCredentials
import com.example.warehouse.service.repository.WarehouseRepo
import kotlinx.coroutines.launch
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance

class AssigmentViewModel(application: Application) : AndroidViewModel(application), DIAware {
    override val di: DI by closestDI(application)
    private val repository: WarehouseRepo by instance()

    suspend fun getAssigments(token: String, email: String): List<Assigment> {
        repository.deleteAssigments()
        return repository.getAssigments(token, email)
    }
    suspend fun loadCredentials(): LoginCredentials? = repository.loadCredentials()
    suspend fun insertCredentials(credentials: LoginCredentials) = repository.insertCredentials(credentials)
    suspend fun loadAssigmentDetails(id: Int): Assigment = repository.loadAssigmentDetails(id)
    fun insertAllAssigments(assigments: List<Assigment>) = viewModelScope.launch {
        return@launch repository.insertAllAssigments(assigments)
    }
    fun doAssigment(token: String, id: Int, assigment: Assigment) = viewModelScope.launch {
        return@launch repository.doAssigment(token, id, assigment)
    }
}