/*
 * Created by Aleksandar Ilić on 11/13/20 8:23 PM
 */

package com.example.warehouse.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(
    @SerializedName(".expires")
    val expires: String,
    @SerializedName(".issued")
    val issued: String,
    @SerializedName("access_token")
    val token: String,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("userName")
    val user: String
) : Parcelable