/*
 * Created by Aleksandar Ilić on 11/13/20 7:51 PM
 */
package com.example.warehouse.service.repository

import com.example.warehouse.service.local.WarehouseDao
import com.example.warehouse.service.model.Assigment
import com.example.warehouse.service.model.LoginCredentials
import com.example.warehouse.service.model.LoginResponse
import com.example.warehouse.service.network.ServiceApi

class WarehouseRepo(private val serviceApi: ServiceApi, private val warehouseDao: WarehouseDao) {
    suspend fun login(grantType: String, usrname: String, pswd: String): LoginResponse = serviceApi.login(grantType, usrname, pswd)
    suspend fun loadCredentials() = warehouseDao.loadCredentials()
    suspend fun insertCredentials(credentials: LoginCredentials) = warehouseDao.insertCredentials(credentials)
    suspend fun getAssigments(token: String, email: String): List<Assigment> = serviceApi.getAssigments(token, email)
    suspend fun loadAssigmentDetails(id: Int): Assigment = warehouseDao.loadAssigmentDetails(id)
    suspend fun insertAllAssigments(assigments: List<Assigment>) = warehouseDao.insertAllAssigments(assigments)
    suspend fun doAssigment(token: String, id: Int, assigment: Assigment) = serviceApi.doAssigment(token, id, assigment)
    suspend fun deleteAssigments() = warehouseDao.deleteAssigments()
}