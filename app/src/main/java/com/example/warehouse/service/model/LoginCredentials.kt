/*
 * Created by Aleksandar Ilić on 11/13/20 9:18 PM
 */

/*
 * Created by Aleksandar Ilić on 11/13/20 6:43 PM
 */

package com.example.warehouse.service.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LoginCredentials(
    @PrimaryKey
    val id: Int,
    var usrname: String,
    var pswd: String,
    var isLoggedIn: Boolean
)
