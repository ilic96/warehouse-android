/*
 * Created by Aleksandar Ilić on 11/13/20 7:16 PM
 */

package com.example.warehouse.service.network

import com.example.warehouse.service.model.Assigment
import com.example.warehouse.service.model.LoginResponse
import retrofit2.http.*

interface ServiceApi {

    @FormUrlEncoded
    @POST("token")
    suspend fun login(@Field("grant_type") grantType: String,
                      @Field("username") username: String,
                      @Field("password") password: String): LoginResponse
    @GET("api/Assigment")
    suspend fun getAssigments(@Header("authorization") token: String,
                              @Query("email") email: String): List<Assigment>
    @PUT("api/Assigment/{id}")
    suspend fun doAssigment(@Header("authorization") token: String,
                            @Path("id") id: Int,
                            @Body assigment: Assigment)


}