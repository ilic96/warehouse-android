
/*
 * Created by Aleksandar Ilić on 11/13/20 6:46 PM
 */

package com.example.warehouse.service.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.warehouse.service.model.Assigment
import com.example.warehouse.service.model.LoginCredentials
import java.security.AccessControlContext

@Database(entities = [LoginCredentials::class, Assigment::class], version = 9, exportSchema = false)
abstract class WarehouseDb : RoomDatabase(){
    abstract fun warehouseDao(): WarehouseDao

    companion object {
        @Volatile
        private var INSTANCE: WarehouseDb? = null

        fun getDatabase(context: Context): WarehouseDb {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, WarehouseDb::class.java, "Warehouse database")
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}