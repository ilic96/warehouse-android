/*
 * Created by Aleksandar Ilić on 11/15/20 10:40 PM
 */

package com.example.warehouse.service.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Assigment(
        @SerializedName("Id")
        @PrimaryKey
        val id: Int,
        @SerializedName("AccountEmail")
        val accountEmail: String,
        @SerializedName("Completed")
        var completed: String,
        @SerializedName("Description")
        val description: String,
        @SerializedName("Name")
        val name: String
) : Parcelable