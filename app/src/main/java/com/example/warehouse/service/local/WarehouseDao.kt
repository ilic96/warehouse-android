/*
 * Created by Aleksandar Ilić on 11/13/20 7:42 PM
 */

/*
 * Created by Aleksandar Ilić on 11/13/20 6:57 PM
 */

package com.example.warehouse.service.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.warehouse.service.model.Assigment
import com.example.warehouse.service.model.LoginCredentials

@Dao
interface WarehouseDao {
    @Query("SELECT id, usrname, pswd, isLoggedIn FROM LoginCredentials")
    suspend fun loadCredentials(): LoginCredentials?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCredentials(credentials: LoginCredentials)

    @Query("SELECT * FROM Assigment WHERE id =:id")
    suspend fun loadAssigmentDetails(id: Int): Assigment

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllAssigments(assigments: List<Assigment>)

    @Query("DELETE FROM Assigment")
    suspend fun deleteAssigments()
}