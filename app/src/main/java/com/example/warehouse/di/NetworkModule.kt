/*
 * Created by Aleksandar Ilić on 11/13/20 7:10 PM
 */

package com.example.warehouse.di

import com.example.warehouse.service.local.WarehouseDb
import com.example.warehouse.service.network.ServiceApi
import com.example.warehouse.service.repository.WarehouseRepo
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = DI.Module("NetModule", false) {

    bind<OkHttpClient>() with singleton { buildOkhttpClient() }
    bind<Retrofit>() with singleton { buildRetrofit(instance()) }
    bind<ServiceApi>() with singleton { buildService(instance()) }
    bind() from singleton { WarehouseDb }
    bind() from singleton { WarehouseDb.getDatabase(instance()).warehouseDao() }
    bind() from singleton { WarehouseRepo(instance(), instance()) }
}
private const val BASE_URL = "http://spaxmk-001-site1.ftempurl.com"

private fun buildOkhttpClient(): OkHttpClient {
    val client = OkHttpClient.Builder()
        .connectionSpecs(listOf(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
    return client.build()
}
private fun buildRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .client(okHttpClient)
    .build()

private fun buildService(retrofit: Retrofit): ServiceApi = retrofit.create(ServiceApi::class.java)